package com.technocorp.portifolio.exception;

public class DeckInexistenteException extends Exception {
    public DeckInexistenteException (String message) {
        super(message);
    }
}
