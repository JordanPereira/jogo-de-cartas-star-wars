package com.technocorp.portifolio.swapiIntegration;

/* import com.technocorp.portifolio.exception.SwapiError; */

import com.technocorp.portifolio.dto.ResponseSwapi;
import lombok.AllArgsConstructor;
import lombok.NoArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.ImportAutoConfiguration;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

@Service
@NoArgsConstructor
@AllArgsConstructor
@ImportAutoConfiguration(RestTemplate.class)
public class DeckStorage {

    @Autowired
    private RestTemplate restTemplate;

    private static final String urlBase = "https://swapi.dev/api/";

    public ResponseSwapi getPeopleList(int page) {
        try {
            String url = urlBase.concat("people/?page=" + page);
            return this.restTemplate.getForObject(url, ResponseSwapi.class);
        } catch (Exception e) {
            System.out.println("ERRO: "+ e.getMessage());
            throw e;
        }
    }

    public ResponseSwapi getStarships(int page) {
        String url = urlBase.concat("starships/?page=" + page);
        return callSwapi(url);
    }

    private ResponseSwapi callSwapi (String url) {
        try {
            return this.restTemplate.getForObject(url, ResponseSwapi.class);
        } catch (Exception e) {
            System.out.println("ERRO: "+ e.getMessage());
            throw e;
        }
    }
}
