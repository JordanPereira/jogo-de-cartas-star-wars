package com.technocorp.portifolio.dto;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class Starships implements Serializable {

    private String name;
    private String starship_class;
    private String cost_in_credits;
    private String length;
    private String crew;
    private String passengers;
    private String MGLT;
    private String cargo_capacity;
}
