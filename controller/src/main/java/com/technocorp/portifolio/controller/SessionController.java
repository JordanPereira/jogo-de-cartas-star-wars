package com.technocorp.portifolio.controller;

import com.technocorp.portifolio.exception.DeckInexistenteException;
import com.technocorp.portifolio.model.ResponseTurn;
import com.technocorp.portifolio.service.StartGameService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/batalha")
public class SessionController {

    @Autowired
    private StartGameService startGameService;

    @GetMapping(path = "/deck")
    @ResponseStatus(HttpStatus.OK)
    public List<String> getListDecks(){
        // new ObjectMapper().readValue();
        return this.startGameService.getDecks();
    }

    @PostMapping(path = "/start")
    @ResponseStatus(HttpStatus.OK)
    public ResponseTurn startSession(@RequestParam String decktype) throws DeckInexistenteException {
        return this.startGameService.startSession(decktype);
    }

    @GetMapping(path = "/")
    @ResponseStatus(HttpStatus.OK)
    public ResponseTurn chooseFeature(@RequestHeader String feature, @RequestHeader String id){
        return this.startGameService.chooseFeature(feature, id);
    }
}
