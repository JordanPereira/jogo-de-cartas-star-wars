package com.technocorp.portifolio.controller;

import com.technocorp.portifolio.dto.DefaultError;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestControllerAdvice;
import org.springframework.web.servlet.mvc.method.annotation.ResponseEntityExceptionHandler;

@RestControllerAdvice
@Slf4j
public class ApplicationExceptionHandler extends ResponseEntityExceptionHandler {

    @ExceptionHandler(IllegalArgumentException.class)
    @ResponseStatus(HttpStatus.NOT_IMPLEMENTED)
    public DefaultError handleIllegalArgumentException(IllegalArgumentException e) {
        log.error("Erro Illegal Argument", e);
        return new DefaultError(HttpStatus.NOT_IMPLEMENTED.value(), e.getMessage(), "");
    }

//    @ExceptionHandler(HttpMessageNotReadableException.class)
//    @ResponseStatus(HttpStatus.NOT_IMPLEMENTED)
//    public DefaultError handleHttpMessageNotReadableException(HttpMessageNotReadableException e) {
//        log.error("Erro Illegal Argument", e);
//        return new DefaultError(HttpStatus.NOT_IMPLEMENTED.value(), e.getMessage(), "");
//    }

    // outras exceptions

    @ExceptionHandler(Exception.class)
    @ResponseStatus(HttpStatus.I_AM_A_TEAPOT)
    public DefaultError handleException(Exception e) {
        log.error("Erro desconhecido", e);
        return new DefaultError(
                HttpStatus.I_AM_A_TEAPOT.ordinal(),
                "Ops! Ocorreu um erro no servidor, a nossa equipe já foi notificada, tente mais tarde",
                e.getMessage()
        );
    }
}
