package com.technocorp.portifolio.controller;

import com.technocorp.portifolio.model.Movie;
import com.technocorp.portifolio.service.MovieService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.Map;

@RestController
@RequestMapping("/movie")
public class MovieController {

    @Autowired
    private MovieService movieService;

    @GetMapping(path = "/search/many_params")
    public Movie searchMovie(@RequestParam Map<String, String> allParams){

        String name = allParams.get("name");
        String genre = allParams.get("genre");
        String description = allParams.get("description");

        return movieService.useBuilder(name, genre, description);
    }

    @GetMapping(path = "/search/name")
    public String[] searchMovie(@RequestParam String name, @RequestParam(required = false) String genre){
        return new String[]{name, genre};
    }

    @DeleteMapping(path = "/delete")
    public int deleteMovie(@RequestHeader int id){
        return id;
    }
}
