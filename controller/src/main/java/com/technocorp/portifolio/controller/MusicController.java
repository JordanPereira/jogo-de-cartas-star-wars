package com.technocorp.portifolio.controller;

import com.technocorp.portifolio.model.Music;
import com.technocorp.portifolio.service.MusicService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;

@RestController
@RequestMapping("/music")
public class MusicController {
    @Autowired
    private MusicService musicService;

    @PostMapping(path = "/add")
    public HttpStatus saveMusic(@Valid @RequestBody(required = true) Music music){
        musicService.insert(music);
        return HttpStatus.CREATED;
    }

    @GetMapping(path = "/list")
    public java.util.List<Music> listMusic(){
        return musicService.findAll();
    }

    @DeleteMapping(path = "/delete")
    public HttpStatus deleteMusic(@RequestHeader String id){
        musicService.delete(id);
        return HttpStatus.OK;
    }

    @GetMapping(path = "/search/{name}")
    public List<Music> searchMusicByName(@PathVariable String name) {
        return musicService.findByName(name);
    }
}
