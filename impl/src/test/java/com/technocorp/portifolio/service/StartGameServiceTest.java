package com.technocorp.portifolio.service;

import com.technocorp.portifolio.dto.ResponseSwapi;
import com.technocorp.portifolio.dto.Starships;
import com.technocorp.portifolio.exception.DeckInexistenteException;
import com.technocorp.portifolio.model.ResponseTurn;
import com.technocorp.portifolio.model.Session;
import com.technocorp.portifolio.repository.SessionRepository;
import com.technocorp.portifolio.swapiIntegration.DeckStorage;
import org.junit.jupiter.api.Disabled;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Comparator;
import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;
import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.BDDMockito.given;

@SpringBootTest(classes = StartGameService.class)
class StartGameServiceTest {

    @MockBean
    private DeckStorage deckStorage;

    @MockBean
    private SessionRepository sessionRepository;

    @Autowired
    private StartGameService startGameService;

    @Test
    public void contextLoads() throws Exception {
        assertThat(startGameService).isNotNull();
    }

    @Test
    void getDecks() {
        List<String> expected = Arrays.asList("films", "people", "planets", "species", "starships", "vehicles");
        assertLinesMatch(expected, startGameService.getDecks());
    }

    @Test
    void startSession() throws DeckInexistenteException {
        // preparação
        Starships starship1 = Starships.builder().starship_class("mock1").crew("teste1").build();
        Starships starship2 = Starships.builder().starship_class("mock2").crew("teste2").build();
        Starships starship3 = Starships.builder().starship_class("mock3").crew("teste3").build();
        Starships[] array = {starship1, starship2, starship3};
        ArrayList<Starships> mockDeck = new ArrayList<>(Arrays.asList(array));
        ResponseSwapi lastResponseSwapi = ResponseSwapi.builder().results(mockDeck).next(null).build();

        String decktype = "starships";

        Session sessionResponse = Session.builder()
                .id("5")
                .deckPlayer1(mockDeck)
                .deckPlayer2(mockDeck)
                .deckPlayer3(mockDeck)
                .turn(1)
                .build();

        //mocks
        given(this.deckStorage.getStarships(1)).willReturn(lastResponseSwapi);
        given(this.sessionRepository.insert(any(Session.class))).willReturn(sessionResponse);

        // comparação
        ResponseTurn responseTurn =  startGameService.startSession(decktype);
        assertEquals(starship1, responseTurn.getChooseFeature());
        assertEquals("5", responseTurn.getIdSession());
        assertEquals(3, responseTurn.getDeck1Length());
        assertEquals(3, responseTurn.getDeck2Length());
        assertEquals(3, responseTurn.getDeck3Length());
    }

    @Disabled
    @Test
    void chooseFeature() {
        assertTrue(true);
    }

    @Test
    void getCards() throws DeckInexistenteException {
        // preparação
        Starships starship1 = Starships.builder().starship_class("mock1").crew("teste1").build();
        Starships starship2 = Starships.builder().starship_class("mock2").crew("teste2").build();
        Starships starship3 = Starships.builder().starship_class("mock3").crew("teste3").build();
        Starships[] array = {starship1, starship2, starship3};
        ArrayList<Starships> mockDeck = new ArrayList<>(Arrays.asList(array));
        ResponseSwapi responseSwapi = ResponseSwapi.builder().results(mockDeck).next("1").build();
        ResponseSwapi lastResponseSwapi = ResponseSwapi.builder().results(mockDeck).next(null).build();

        ArrayList<Starships> expected = new ArrayList<>();
        expected.addAll(mockDeck);
        expected.addAll(mockDeck);
        expected.addAll(mockDeck);
        expected.addAll(mockDeck);

        // mock
        given(this.deckStorage.getStarships(1)).willReturn(responseSwapi);
        given(this.deckStorage.getStarships(2)).willReturn(responseSwapi);
        given(this.deckStorage.getStarships(3)).willReturn(responseSwapi);
        given(this.deckStorage.getStarships(4)).willReturn(lastResponseSwapi);

        // assert
        assertIterableEquals(startGameService.getCards("starships"), expected);
    }

    @Test
    void getCardsException() {
        // assert
        assertThrows(DeckInexistenteException.class, () -> {
            startGameService.getCards("star");
        });
    }

    @Test
    void shuffeCards() {
        // preparação
        Starships starship1 = Starships.builder().starship_class("mock1").crew("teste1").build();
        Starships starship2 = Starships.builder().starship_class("mock2").crew("teste2").build();
        Starships starship3 = Starships.builder().starship_class("mock3").crew("teste3").build();
        List<Starships> list = Arrays.asList(starship1, starship2, starship3);

        // assert
        List<Starships> response = startGameService.shuffeCards(list);
        // assertNotEquals(list, response);
        response.sort(Comparator.comparing(Starships::getCrew));
        list.sort(Comparator.comparing(Starships::getCrew));

        assertIterableEquals(list, response);
    }

    @Disabled
    @Test
    void dealCards() {
    }
}