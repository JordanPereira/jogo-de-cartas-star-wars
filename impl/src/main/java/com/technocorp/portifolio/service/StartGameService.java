package com.technocorp.portifolio.service;

import com.technocorp.portifolio.dto.ResponseSwapi;
import com.technocorp.portifolio.dto.Starships;
import com.technocorp.portifolio.exception.DeckInexistenteException;
import com.technocorp.portifolio.model.ResponseTurn;
import com.technocorp.portifolio.model.Session;
import com.technocorp.portifolio.repository.SessionRepository;
import com.technocorp.portifolio.swapiIntegration.DeckStorage;
import lombok.AllArgsConstructor;
import lombok.NoArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.*;

@Service
@NoArgsConstructor
@AllArgsConstructor
public class StartGameService {

    @Autowired
    private DeckStorage deckStorage;

    @Autowired
    private SessionRepository sessionRepository;

    public  List<String> getDecks() {
        return Arrays.asList("films", "people", "planets", "species", "starships", "vehicles");
    }

    protected List<Starships> getCards(String decktype) throws DeckInexistenteException {
        if (!decktype.equals("starships")){
            // throw new IllegalArgumentException("Deck não disponível");
            throw new DeckInexistenteException("Deck não existe");
            // int result = 2 / 0;
        }
        ArrayList<Starships> deck = new ArrayList<>();
        ResponseSwapi responseSwapi;
        int i = 1;
        do {
            responseSwapi = deckStorage.getStarships(i);
            deck.addAll(responseSwapi.getResults());
            i++;
        }
        while (responseSwapi.getNext() != null);
        return deck;
    }

    protected List<Starships> shuffeCards (List<Starships> deck){
        Collections.shuffle(deck);
        return deck;
    }

    protected Session dealCards (List<Starships> deck, int numberOfPlayers) {
        int numberOfCards = deck.toArray().length / numberOfPlayers;

        List<Starships> deck1 = deck.subList(0, numberOfCards);
        List<Starships> deck2 = deck.subList(numberOfCards, numberOfCards * 2 );
        List<Starships> deck3 = deck.subList(numberOfCards * 2 , numberOfCards * 3);
        Session newSession = Session.builder().deckPlayer1(deck1).deckPlayer2(deck2).deckPlayer3(deck3).turn(1).build();

        return sessionRepository.insert(newSession);
    }

    public ResponseTurn startSession(String decktype) throws DeckInexistenteException {

        List<Starships> deck = getCards(decktype);
        List<Starships> shuffeDeck = shuffeCards(deck);
        Session session = dealCards(shuffeDeck, 3);
        try {
            return ResponseTurn.builder()
                    .idSession(session.getId())
                    .chooseFeature(session.getDeckPlayer1().get(0))
                    .deck1Length(session.getDeckPlayer1().size())
                    .deck2Length(session.getDeckPlayer2().size())
                    .deck3Length(session.getDeckPlayer3().size())
                    .build();
        } catch (IndexOutOfBoundsException e){
            throw new DeckInexistenteException("Deck Vazio!");
        }
    }

    public ResponseTurn chooseFeature(String feature, String id){
        Session currentSession = (sessionRepository.findById(id)).orElseThrow( NoSuchFieldError::new );
        Starships deck1 =  currentSession.getDeckPlayer1().get(0);
        Starships deck2 =  currentSession.getDeckPlayer2().get(0);
        Starships deck3 =  currentSession.getDeckPlayer3().get(0);


        return ResponseTurn.builder().build();
    }
}
