package com.technocorp.portifolio.service;

import com.technocorp.portifolio.model.Movie;
import lombok.AllArgsConstructor;
import org.springframework.stereotype.Service;

@Service
@AllArgsConstructor
public class MovieService {

    public Movie useBuilder(String name, String genre, String description) {
        return Movie.builder().genre(genre).description(description).name(name).build();
    }

}
