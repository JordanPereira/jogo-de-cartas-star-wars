package com.technocorp.portifolio.service;

import com.technocorp.portifolio.model.Music;
import com.technocorp.portifolio.repository.MusicRepository;
import lombok.AllArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
@AllArgsConstructor
public class MusicService {

    @Autowired
    private final MusicRepository musicRepository;

    public void insert(Music music) { musicRepository.insert(music); }

    public List<Music> findAll() {
        return musicRepository.findAll();
    }

    public void delete(String id) { musicRepository.deleteById(id); }

    public List<Music> findByName(String name) { return musicRepository.findByName(name); }

//    public Optional<Music> findById(String id) { return musicRepository.findById(id); }
}
