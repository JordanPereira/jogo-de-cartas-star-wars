package com.technocorp.portifolio.repository;

import com.technocorp.portifolio.model.Music;
import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface MusicRepository extends MongoRepository<Music, String> {

    List<Music> findByName(String name);

    // Music insert(Music music);

    List<Music> findAll();

    void deleteById(String id);
}
