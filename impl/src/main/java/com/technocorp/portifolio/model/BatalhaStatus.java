package com.technocorp.portifolio.model;

import lombok.Builder;
import lombok.Data;

import java.util.List;

@Data
@Builder
public class BatalhaStatus {
    private Integer vida;
    private Integer vidaOponente;
    private List<Cartas> mesa;
    private List<Cartas> mesaOponente;
    private Integer mana;
    private Integer manaOponente;
    private List<Cartas> mao;
}
