package com.technocorp.portifolio.model;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.experimental.SuperBuilder;
import org.springframework.stereotype.Component;

import java.util.Date;

@Component
@AllArgsConstructor
@NoArgsConstructor
@SuperBuilder
public abstract class Media {
    @Getter private String id;
    @Getter @Setter private String name;
    @Getter @Setter private Long duration;
    @Getter @Setter private String[] artists;
    @Getter @Setter private Date publication;
}
