package com.technocorp.portifolio.model;

import lombok.Builder;
import lombok.Data;

@Data
@Builder
public class Cartas {
    private String nome;
    private Integer ataque;
    private Integer resistencia;
    private Boolean voar;
    private Boolean atacaVoar;
    private Integer mana;
}
