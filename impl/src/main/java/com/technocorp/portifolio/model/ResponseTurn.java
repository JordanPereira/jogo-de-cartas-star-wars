package com.technocorp.portifolio.model;

import com.technocorp.portifolio.dto.Starships;
import lombok.Builder;
import lombok.Data;

@Data
@Builder
public class ResponseTurn {

    private String idSession;
    private Starships chooseFeature;
    private Starships card1;
    private Starships card2;
    private Starships card3;
    private int deck1Length;
    private int deck2Length;
    private int deck3Length;
}
