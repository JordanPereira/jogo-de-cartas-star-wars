package com.technocorp.portifolio.model;

import com.technocorp.portifolio.dto.Starships;
import lombok.Builder;
import lombok.Data;
import lombok.Getter;

import java.util.List;

@Data
@Builder
public class Session {
    @Getter private String id;

    private int turn;

    private List<Starships> deckPlayer1;
    private List<Starships> deckPlayer2;
    private List<Starships> deckPlayer3;
}
